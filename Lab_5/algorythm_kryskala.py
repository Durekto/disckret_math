from typing import List, Tuple, Optional
import json


def get_num_input(message):
    repeat = True

    while repeat:
        repeat = False

        try:
            inp = input(message)
            inp = int(inp)
        except Exception:
            print("Expected an int number, got {}, try again\n".format(type(inp)))
            repeat = True

    return inp


def get_matrix_of_adyacencia() -> List[List[float]]:
    vertices_count = get_num_input("Enter number of vertices: ")

    matrix: List[List[float]] = []

    # Fill default
    for row in range(vertices_count):
        matrix.append([])
        for col in range(vertices_count):
            matrix[row].append(0)

    for row in range(vertices_count):
        for col in range(vertices_count):

            if matrix[row][col] == 0:
                value = get_num_input(
                    "Enter the weight of the edge between {}-{} vertices: ".format(row, col))
                matrix[row][col] = value
                matrix[col][row] = value

    return matrix


def get_matrix_from_file(file) -> List[List[float]]:
    with open(file) as f:
        matrix = json.load(f)["matrix"]

    return matrix


def find_min_unvisited_edge(matrix, pertence) -> Tuple[float, Optional[int], Optional[int]]:
    """ Finds the edge with the minimal weight

    :return: tuple where first element is weight, second first vertex and third is second vertex
    """
    vertices = len(matrix)

    min = float("inf")
    vertexA = None
    vertexB = None

    for row in range(vertices):
        for col in range(vertices):
            # If two vertices doesn't belong to the same tree, and have minimal weight
            if matrix[row][col] != 0 and pertence[col] != pertence[row] and matrix[row][col] < min:
                min = matrix[row][col]
                vertexA = row
                vertexB = col

    return min, vertexA, vertexB


def find_spanning_graph(matrix: List[List[float]]) -> List[List[float]]:
    spanning_graph: List[List[float]] = []
    # Store index of a tree to which a vertex belongs
    pertenece: List[int] = []
    vertices_added = 0

    # Init
    for row in range(len(matrix)):
        spanning_graph.append([])
        pertenece.append(row)
        for col in range(len(matrix)):
            spanning_graph[row].append(0)

    while vertices_added < len(matrix):
        # Find edge with the minimal weight
        edge = find_min_unvisited_edge(matrix, pertenece)

        # Add edge to the spanning graph
        # and change index of all vertices from the tree of vertex B to index of the tree of vertex A pertenece
        # (Add all vertices from the tree of a vertex B to the tree of vertex A)
        if edge[1] is None or edge[2] is None:
            break

        row = edge[1]
        col = edge[2]
        value = edge[0]
        spanning_graph[row][col] = value
        spanning_graph[col][row] = value

        old_tree_index = pertenece[col]
        new_tree_index = pertenece[row]
        for vertex, tree_index in enumerate(pertenece):
            if tree_index == old_tree_index:
                pertenece[vertex] = new_tree_index

        vertices_added += 1

    return spanning_graph


def print_matrix(matrix, caption):
    print(caption, end="\n\n")
    vertices = len(matrix)

    print("    ", end="")
    for row in range(vertices + 2):
        if row == 1:
            for i in range(vertices * 3 + 4):
                print("-", end="")
        elif row == 0:
            for i in range(vertices):
                print("{:3}".format(i+1), end="")
        else:
            for col in range(vertices + 2):
                if col == 1:
                    print("|", end="")
                elif col == 0:
                    print("{:3}".format(row-1), end="")
                else:
                    print("{:3}".format(matrix[row-2][col-2]), end="")

        print()

    print("\n\n")


# Run script
if __name__ == "__main__":
    matrix = get_matrix_from_file("matrix.json")
    print()
    print_matrix(matrix, "Given graph")
    matrix = find_spanning_graph(matrix)
    print_matrix(matrix, "Spanning graph")
