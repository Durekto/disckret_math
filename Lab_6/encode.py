
def getMessageData(message):
    alphabet = []
    totalLeters = 0
    lettersCount = []
    probability = []

    for char in message:
        totalLeters += 1

        if char in alphabet:
            index = alphabet.index(char)
            lettersCount[index] += 1
        else:
            index = len(alphabet)
            alphabet.append(char)
            lettersCount.append(1)

    for idx, count in enumerate(lettersCount):
        probability.append((idx, round(count/totalLeters, 2)))

    return (alphabet, probability)


def getAlphabetByFanoAlg(probability):
    probability = sortByProbability(probability)
    newAlphabet = {}
    for idx in range(len(probability)):
        newAlphabet[idx] = ""

    def appendRange(arr, start, count, symbol):
        for idx in range(count):
            arr[idx+start] += symbol

    def digInto(context, alphabet, start, count):
        if len(context) == 1:
            return

        centre = len(context)//2

        appendRange(alphabet, start, centre, "0")
        digInto(context[:centre], alphabet, start, centre)

        appendRange(alphabet, start+centre, count - centre, "1")
        digInto(context[centre:], alphabet, start+centre, count-centre)

    digInto(probability, newAlphabet, 0, len(probability))

    return newAlphabet


def getAlphabetByHoffmanAlg(probability):
    probability = sortByProbability(probability, False)
    newAlphabet = {}

    def combine(first, second):
        return (first, first[1] + second[1], second)

    def traverse(node, path):
        if (len(node) == 2):
            newAlphabet[node[0]] = path
            return

        traverse(node[0], path + "1")
        traverse(node[2], path + "0")

    def digInto(context):
        if (len(context) == 1):
            return context

        combinedNode = combine(context[-1], context[-2])
        context = context[:-2]
        context.append(combinedNode)
        context = sortByProbability(context, False)
        context = digInto(context)

        return context

    traverse(digInto(probability)[0], "")
    return newAlphabet


def sortByProbability(probability, ascending=True):
    return sorted(probability, key=lambda entry: entry[1] if ascending else -entry[1])


def combineAlphabets(old, new):
    newDict = {}

    for idx, val in new.items():
        newDict[old[idx]] = val

    return newDict


def translateMessage(message, alphabet):
    translatedMessage = ""

    for char in message:
        translatedMessage += alphabet[char] + " "

    return translatedMessage


messages = ["КОТИГОРОШКО", "THIS IS A SIMPLE EXAMPLE OF HUFFMAN ENCODING."]

for message in messages:
    print()
    print()
    print("Translating message: '" + message + "'")
    print()

    alphabet, probability = getMessageData(message)
    print("Probabilities: " + str(probability))
    hoffAlphabet = combineAlphabets(alphabet, getAlphabetByHoffmanAlg(probability))
    fanoAlphabet = combineAlphabets(alphabet, getAlphabetByFanoAlg(probability))
    print("Hoffman alphabet: " + str(hoffAlphabet))
    print("Fano alphabet: " + str(fanoAlphabet))
    print()

    print("Translated by Hoffman algo: " + translateMessage(message, hoffAlphabet))
    print("Translated by Fano algo: " + translateMessage(message, fanoAlphabet))
