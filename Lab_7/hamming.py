import math


def num_to_bin(num):
    bites = ""

    while num != 0:
        bites += str(num % 2)
        num = math.floor(num/2)

    bites = bites[::-1]
    # UTF-8
    while len(bites) < 16:
        bites = "0" + bites

    return bites


def message_to_bin_message(message):
    binnary = ""

    for char in message:
        binnary += num_to_bin(ord(char))

    return binnary


def get_parity_bits_count(data_bits_count):
    for i in range(data_bits_count):
        if 2**i >= data_bits_count + 1:
            return i


def insert_parity_bits(data, parity_bits_count):
    data = "0"+data

    for i in range(1, parity_bits_count):
        pos = 2**i-1
        data = data[:pos] + "0" + data[pos:]

    return data


def calculate_parity_bits(data, parity_bits_count):
    bits = len(data)

    for parity_bit in range(parity_bits_count):
        pos = 2**parity_bit
        actual_pos = pos - 1
        bits_sum = 0
        bit = actual_pos

        while bit < bits:

            # update bits
            for _ in range(pos):
                if bit >= bits:
                    break
                bits_sum += int(data[bit])
                bit += 1

            if bit >= bits:
                break

            # skip bits
            for _ in range(pos):
                bit += 1

        data = data[:actual_pos] + str(bits_sum % 2) + data[actual_pos+1:]

    return data


message = message_to_bin_message("КОТИГОРОШКО")

for block in range(len(message) // 4):
    message_block = message[block*4: (block+1)*4]
    parity_bits = get_parity_bits_count(len(message_block))
    inserted_parity_bits = insert_parity_bits(message_block, parity_bits)
    encodedMessage = calculate_parity_bits(inserted_parity_bits, parity_bits)
    print(encodedMessage, end="_")
