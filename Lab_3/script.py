import json


def traverse_graph(graph):
    new_graph = []

    for row in range(len(graph)):
        rows_to_add = [row]

        for col in range(len(graph[0])):
            if graph[row][col] and row != col:
                rows_to_add.append(col)

        new_graph.append(add_list_of_rows(graph, rows_to_add))

    return new_graph


def add_list_of_rows(graph, rows):
    if not rows:
        return []

    result_row = graph[rows[0]]

    for row_to_add in rows[1:]:
        result_row = add_rows(result_row, graph[row_to_add])

    return result_row


def add_rows(first, second):
    result_row = []

    for elem in range(len(first)):
        result_row.append(first[elem] or second[elem])

    return result_row


graph = None
with open("graph.json", "r") as file:
    graph = json.load(file)

with open("output.json", "w") as file:
    json.dump(traverse_graph(graph), file)
